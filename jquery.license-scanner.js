(function($) {
    $.fn.licenseScanner = function(options) {

        var settings = $.extend(true, {}, $.fn.licenseScanner.defaults, options);
        var pressed = false,
            chars = [];

        this.keypress(function(e) {
            // chars.push(String.fromCharCode(e.which))
            if(e.which == 64) {
                chars.push(String.fromCharCode(e.which));
            }else if(chars.length > 1 && chars[1] == String.fromCharCode(13)){
                if(e.which != 13){
                    chars.push(String.fromCharCode(e.which));
                }else {
                    if(chars.length > 0) {
                        var _info_dict = process(chars);
                        if(typeof settings.onScan === "function"){
                            settings.onScan.call(this, _info_dict);
                        }
                        chars = [];
                    }
                    return false;
                }
            }else if(chars[0] == "@"){
                chars.push(String.fromCharCode(e.which));
            }
        });
    };
    var key = [
                'DCA', 'DCB', 'DCD', 'DBA', 'DCS',
                'DAC', 'DAD', 'DBD', 'DBB', 'DBC',
                'DAY', 'DAU', 'DAG', 'DAI', 'DAJ',
                'DAK', 'DAQ', 'DCF', 'DCG'
        ],
        trunc_key = ['DDE', 'DDF', 'DDG'],
        optional_key = [
            'DAH', 'DAZ', 'DCI',
            'DCJ', 'DCK', 'DBN', 'DBG', 'DBS',
            'DCU', 'DCE', 'DCL', 'DCM', 'DCN',
            'DCO', 'DCP', 'DCQ', 'DCR', 'DDA',
            'DDB', 'DDC', 'DDD', 'DAW', 'DAX',
            'DDH', 'DDI', 'DDJ', 'DDK', 'DDL'
        ],
        info_dict = {};
    function process(arr) {
        spliced_arr = arr.slice(41)
        joined_string = spliced_arr.join("")
        key.forEach(function(el, index) {

            var regex = new RegExp(el),
                position = joined_string.search(regex),
                stop_position;

            if(key.length > index + 1){
                var next_regex = new RegExp(key[index + 1]);
                stop_position = joined_string.search(next_regex);
            }else{
                stop_position = joined_string.search(trunc_key[0]);
            }
            if (position > -1){
                info_dict[el] = joined_string.substring(position + el.length, stop_position)
            }
        });

        trunc_key.forEach(function(el, index) {

            var regex = new RegExp(el),
                position = joined_string.search(regex),
                stop_position = position + 4;

            if (position > -1){
                info_dict[el] = joined_string.substring(position + el.length, stop_position)
            }
        });

        var actual_keys = [],
            actual_position = -1;
        optional_key.forEach( function(el, index) {
            var regex = new RegExp(el),
                position = joined_string.search(regex);
            if (position > -1){
                if (actual_position == -1){
                    actual_position = position;
                    actual_keys.push(el);
                }else if (position > actual_position){
                    actual_keys.push(el);
                }else{
                    actual_keys.unshift(el);
                }
            }
        });

        actual_keys.forEach(function(el, index) {

            var regex = new RegExp(el),
                position = joined_string.search(regex),
                stop_position;

            if(actual_keys.length > index + 1){
                var next_regex = new RegExp(actual_keys[index + 1]);
                stop_position = joined_string.search(next_regex);
            }else{
                stop_position = joined_string.search(/DDD/);
            }
            if (position > -1){
                info_dict[el] = joined_string.substring(position + el.length, stop_position)
            }
        });
        copy_dict = info_dict;
        info_dict = {};
        return copy_dict
    }

    $.fn.licenseScanner.defaults = {
        onScan: function(dictionary){
            console.log(dictionary);
        },
    }
}(jQuery))