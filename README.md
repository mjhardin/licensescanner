# jQuery.licenseScanner

jQuery.licenseScanner is a jQuery plugin that will parse information after using a barcode scanner to scan the 2d barcode on the back of a driver's license or other US identification.

## Usage

To use:

```
#!javascript

// Initialze the listener
$(window).licenseScanner();

//Scanning a barcode should log an object to console that looks like this
//{
//    DAC: "HANK"
//    DAD: "RUTHERFORD"
//    DAG: "84 RAINEY ST"
//    DAI: "ARLEN"
//    DAJ: "TX"
//    DAK: "780540000  "
//    DAQ: "80976859"
//    DAU: "072 IN"
//    DAW: "220"
//    DAY: "BRN"
//    DAZ: "BRN"
//    DBA: "04191963"
//    DBB: "04192017"
//    DBC: "1"
//    DBD: "10062011"
//    DCA: "C"
//    DCB: "NONE"
//    DCD: "NONE"
//    DCF: "10/06/201151522/AAFD/16"
//    DCG: "USA"
//    DCK: "11279809768590401"
//    DCS: "HILL"
//    DDB: "04162010"
//    DDD: "DDD"
//    DDE: "U"
//    DDF: "U"
//    DDG: "U"
//}

```

## Options

```
#!javascript


$(window).licenseScanner({
    onScan: function(dictionary){
        // This will print only the license holder's first name to console.
        console.log(dictionary[DAC])
    }
});
```